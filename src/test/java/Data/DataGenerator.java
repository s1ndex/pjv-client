package Data;

import java.nio.charset.Charset;
import java.util.Random;

public class DataGenerator {
    public static String GenerateString(int length){
        byte[] array = new byte[length];
        Random random = new Random();
        random.nextBytes(array);
        String result = new String(array, Charset.forName("UTF-8"));

        return result;
    }

    public static int GenerateInt(){
        Random random = new Random();
        int result = random.nextInt();

        return result;
    }

    public static long GenerateLong(){
        Random random = new Random();
        long result = random.nextLong();

        return result;
    }
}
