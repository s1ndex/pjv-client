package UnitTests;

import model.Book;
import model.Database;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static Data.DataGenerator.*;
import static org.mockito.Mockito.*;

public class DatabaseTests {

    @Test
    public void ConversionDateIsNotNullTest(){
        // Arrange
        Book book = mock(Book.class);

        int expectedId = GenerateInt();
        when(book.getBookId()).thenReturn(expectedId);

        String expectedName = GenerateString(7);
        when(book.getBookName()).thenReturn(expectedName);

        String expectedShelf = GenerateString(7);
        when(book.getBookShelf()).thenReturn(expectedShelf);

        String expectedISBN = GenerateString(7);
        when(book.getISBN()).thenReturn(expectedISBN);

        String expectedSerialNumber = GenerateString(7);
        when(book.getSerialNumber()).thenReturn(expectedSerialNumber);

        Date expectedDate = new Date(GenerateLong());
        when(book.getPublishDate()).thenReturn(expectedDate);

        List<Book> list = new ArrayList<>();
        list.add(book);

        Database base = new Database();
        base.setBooks(list);

        // Act
        Object[][] result = base.getBooks();

        // Assert
        Assertions.assertEquals(expectedName, result[0][1]);
        Assertions.assertEquals(expectedISBN, result[0][2]);
        Assertions.assertEquals(expectedShelf, result[0][3]);
        Assertions.assertEquals(expectedSerialNumber, result[0][4]);
        Assertions.assertEquals(expectedDate, result[0][5]);
    }

    @Test
    public void ConversionDateIsNullTest(){
        // Arrange
        Book book = mock(Book.class);

        int expectedId = GenerateInt();
        when(book.getBookId()).thenReturn(expectedId);

        String expectedName = GenerateString(7);
        when(book.getBookName()).thenReturn(expectedName);

        String expectedShelf = GenerateString(7);
        when(book.getBookShelf()).thenReturn(expectedShelf);

        String expectedISBN = GenerateString(7);
        when(book.getISBN()).thenReturn(expectedISBN);

        String expectedSerialNumber = GenerateString(7);
        when(book.getSerialNumber()).thenReturn(expectedSerialNumber);

        String expectedDate = "";
        when(book.getPublishDate()).thenReturn(null);

        List<Book> list = new ArrayList<>();
        list.add(book);

        Database base = new Database();
        base.setBooks(list);

        // Act
        Object[][] result = base.getBooks();

        // Assert
        Assertions.assertEquals(expectedName, result[0][1]);
        Assertions.assertEquals(expectedISBN, result[0][2]);
        Assertions.assertEquals(expectedShelf, result[0][3]);
        Assertions.assertEquals(expectedSerialNumber, result[0][4]);
        Assertions.assertEquals(expectedDate, result[0][5]);
    }
}
