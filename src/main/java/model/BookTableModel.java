package model;

import javax.swing.table.DefaultTableModel;

public class BookTableModel extends DefaultTableModel {

    private static String[] colNames = {
            "ID", "Name", "ISBN", "Book Shelf",
            "Serial Number", "Publish Date"
    };

    /**
     * Table model which table using to present visual data
     *
     * @param dbConv data vector
     */
    public BookTableModel(Object[][] dbConv) {
        super(dbConv, colNames);
    }

    /**
     * Sets data vector of a model
     *
     * @param data
     */
    public void setData(Object[][] data) {
        setRowCount(0);

        for (Object[] o : data) {
            this.addRow(o);
        }
    }
}
