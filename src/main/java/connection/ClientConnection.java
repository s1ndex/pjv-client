package connection;

import model.Book;

import java.io.*;
import java.net.*;
import java.util.List;
import java.util.logging.Logger;

public class ClientConnection {

    private final static Logger LOGGER = Logger.getLogger(ClientConnection.class.getName());

    private static Socket connection;
    private static ObjectInputStream input;
    private static ObjectOutputStream output;

    public ClientConnection() {
    }

    /**
     * Opening connection socket with
     *
     * @param serverAddress address
     * @param port and port
     * @throws IOException
     */
    public static void connect(String serverAddress, int port) throws IOException {
        connection = new Socket(serverAddress, port);
    }

    /**
     * Closing connection socket
     *
     * @throws IOException
     */
    public static void disconnect() throws IOException {
        connection.close();
    }

    /**
     * Setting up I/O streams
     *
     * @throws IOException
     */
    public static void setupStreams() throws IOException {
        if (input == null || output == null) {
            output = new ObjectOutputStream(connection.getOutputStream());
            input = new ObjectInputStream(connection.getInputStream());
            output.flush();
        }
    }

    /**
     * Closing I/O streams
     *
     * @throws IOException
     */
    public static void closeStreams() throws IOException {
        output.close();
        input.close();
    }

    /**
     * Send String request to server
     *
     * @param request request
     * @return List of Books
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static List<Book> sendBookRequest(String request) throws IOException, ClassNotFoundException {
        output.writeObject(request);
        output.flush();
        List<Book> answer = (List<Book>) input.readObject();

        return answer;
    }

    /**
     * Sending simple request to server
     *
     * @param request request
     * @throws IOException
     */
    public static void sendRequest(String request) throws IOException {
        output.writeObject(request);
        output.flush();
    }

    /**
     * Asking server for Client's id
     *
     * @param request request
     * @return user id
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static int getUserID(String request) throws IOException, ClassNotFoundException {
        output.writeObject(request);
        output.flush();
        int id = (int) input.readObject();

        return id;
    }

    /**
     * Request for checking validity of user's login data
     *
     * @param username
     * @param password
     * @return true if login data is valid, false otherwise
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static boolean passwordCheck(String username, String password) throws IOException, ClassNotFoundException {
        output.writeObject("LOGIN " + username + " " + password);
        output.flush();
        boolean answer = (boolean) input.readObject();
        LOGGER.info("CLIENT: Login attempt is: " + answer);
        return answer;
    }

    /**
     * Checking availability of server
     *
     * @param hostName server's address
     * @return true if connection is available, false otherwise
     * @throws IOException
     */
    public static boolean isSocketAlive(String hostName) throws IOException {
        try {
            return InetAddress.getByName(hostName).isReachable(1000);
        } catch (UnknownHostException e) {
            LOGGER.severe("CLIENT: Possibly wrong port number");
        }

        return false;
    }

    public static void setConnection(Socket connection) {
        ClientConnection.connection = connection;
    }
}
