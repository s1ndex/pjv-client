package gui.listeners;

import java.awt.event.ActionEvent;

public interface GoFrameListener {

    /**
     * Changing frame by occurred event
     *
     * @param event
     */
    public void moveToFrame(ActionEvent event);
}
