package gui.listeners;

public interface BookTableListener {

    /**
     * Listener's method asking to borrow book by its id
     *
     * @param bookId
     */
    public void rowBorrowed(int bookId);

    /**
     * Listener's method asking to return book by its id
     *
     * @param bookId
     */
    public void rowReturned(int bookId);
}
