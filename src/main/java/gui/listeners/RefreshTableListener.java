package gui.listeners;

public interface RefreshTableListener {

    /**
     * Asking table to refresh its data
     */
    public void refreshBookTable();
    public void refreshBorrowTable();
}
