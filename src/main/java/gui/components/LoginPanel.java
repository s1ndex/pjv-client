package gui.components;

import connection.ClientConnection;
import gui.events.LoginEvent;
import gui.listeners.LoginListener;
import gui.listeners.PrefsListener;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.logging.Logger;

public class LoginPanel extends JPanel {

    private final static Logger LOGGER = Logger.getLogger(LoginPanel.class.getName());

    private JLabel userNameLabel;
    private JTextField usernameField;

    private JLabel passwordLabel;
    private JPasswordField passwordField;

    private JLabel addressLabel;
    private JComboBox addressComboBox;

    private JLabel portLabel;
    private JSpinner portSpinner;
    private SpinnerNumberModel spinnerModel;

    private JButton connectButton;
    private JButton exitButton;

    private PrefsListener prefsListener;
    private LoginListener loginListener;

    public LoginPanel() {
        Dimension dim = getPreferredSize();

        dim.width = 300;
        setPreferredSize(dim);

        userNameLabel = new JLabel("Username: ");
        usernameField = new JTextField(12);

        passwordLabel = new JLabel("Password: ");
        passwordField = new JPasswordField(12);

        addressLabel = new JLabel("IP-address: ");
        addressComboBox = new JComboBox();

        portLabel = new JLabel("Port: ");
        spinnerModel = new SpinnerNumberModel(65100, 0, 65535, 1);
        portSpinner = new JSpinner(spinnerModel);

        connectButton = new JButton("Connect");
        exitButton = new JButton("Exit");

        // ====== Set up buttons ======

        connectButton.setMnemonic(KeyEvent.VK_C);
        exitButton.setMnemonic(KeyEvent.VK_X);

        // ====== Set up Password field ======

        passwordField.setEchoChar('▲');

        // ====== Set up Address Combo Box ======

        DefaultComboBoxModel addressModel = new DefaultComboBoxModel();
        addressModel.addElement("localhost");
        addressModel.addElement("129.14.31.112");
        addressModel.addElement("U.S. Army HQ");
        addressComboBox.setModel(addressModel);
        addressComboBox.setSelectedIndex(0);
        addressComboBox.setEditable(true);

        // ====== Set listener on top of Connection button ======

        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String username = usernameField.getText();
                String password = new String(passwordField.getPassword());
                String address = (String) addressComboBox.getSelectedItem();
                Integer port = (Integer) portSpinner.getValue();

                if (prefsListener != null) {
                    prefsListener.preferencesSet(
                            username, password, address, port
                    );
                }

                LoginEvent event = new LoginEvent(
                        this, username, password, address, port);

                if (loginListener != null && !address.equals("U.S. Army HQ")) {
                    try {
                        loginListener.loginEventOccured(event);
                    } catch (IOException ex) {
                        LOGGER.severe("CLIENT: Error while sending message to the server!");
                    } catch (ClassNotFoundException ex) {
                        LOGGER.severe("CLIENT: There's no such class Book found");
                    }
                } else {
                    JOptionPane.showMessageDialog(
                            LoginPanel.this,
                            "Server isn't available at the moment.",
                            "Error",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        // ====== Set listener on top of Exit button ======

        exitButton.addActionListener(e -> {
            try {
                ClientConnection.sendRequest("QUIT");
                ClientConnection.disconnect();
                ClientConnection.closeStreams();
            } catch (Exception ex) {
                LOGGER.severe("CLIENT: Connection was lost");
            } finally {
                System.exit(0);
            }
        });

        layoutComponents();
        setVisible(true);
    }

    private void layoutComponents() {
        JPanel controlsPanel = new JPanel();
        JPanel buttonsPanel = new JPanel();

        int space = 10;
        Border spaceBorder = BorderFactory.createEmptyBorder(space, space, space, space);
        Border titleBorder = BorderFactory.createTitledBorder("Log In");

        controlsPanel.setBorder(BorderFactory.createCompoundBorder(spaceBorder, titleBorder));

        controlsPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        gc.gridy = 0;
        Insets rightPadding = new Insets(0, 0, 0, 15);
        Insets noPadding = new Insets(0, 0, 0, 0);
        // ====== First row ======

        gc.weighty = 1;
        gc.weightx = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        controlsPanel.add(userNameLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        controlsPanel.add(usernameField, gc);

        // ====== Next row ======

        gc.gridy++;

        gc.weighty = 1;
        gc.weightx = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        controlsPanel.add(passwordLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        controlsPanel.add(passwordField, gc);

        // ====== Next row ======

        gc.gridy++;

        gc.weighty = 1;
        gc.weightx = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        controlsPanel.add(addressLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        controlsPanel.add(addressComboBox, gc);

        // ====== Next row ======

        gc.gridy++;

        gc.weighty = 1;
        gc.weightx = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        controlsPanel.add(portLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        controlsPanel.add(portSpinner, gc);

        // ====== Buttons Panel ======

        buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        buttonsPanel.add(connectButton);
        buttonsPanel.add(exitButton);

        Dimension btnSize = connectButton.getPreferredSize();
        exitButton.setPreferredSize(btnSize);

        // ====== Add sub panels to dialog ======

        setLayout(new BorderLayout());
        add(controlsPanel, BorderLayout.CENTER);
        add(buttonsPanel, BorderLayout.SOUTH);
    }

    /**
     * Saves your default data at login frame which you have entered last time
     *
     * @param user
     * @param pass
     * @param address
     * @param port
     */
    public void setDefaults(String user, String pass, String address, int port) {
        usernameField.setText(user);
        passwordField.setText(pass);
        portSpinner.setValue(port);

        if (address.equals("localhost")) {
            addressComboBox.setSelectedIndex(0);
        } else if (address.equals("U.S. Army HQ")) {
            addressComboBox.setSelectedIndex(2);
        } else {
            addressComboBox.setSelectedIndex(1);
        }
    }

    /**
     * Listener for default data on login frame
     *
     * @param listener
     */
    public void setPrefsListener(PrefsListener listener) {
        this.prefsListener = listener;
    }

    /**
     * Login listener which helps to login to the system
     *
     * @param listener
     */
    public void setLoginListener(LoginListener listener) {
        this.loginListener = listener;
    }
}
