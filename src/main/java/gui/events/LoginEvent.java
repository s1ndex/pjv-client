package gui.events;

import java.util.EventObject;

public class LoginEvent extends EventObject {

    private String username;
    private String password;
    private String address;
    private int port;

    /**
     * Constructs a prototypical Event while user logining in
     *
     * @param source is LoginPanel
     * @param username
     * @param password
     * @param address
     * @param port
     */
    public LoginEvent(Object source, String username, String password, String address, int port) {
        super(source);
        this.username = username;
        this.password = password;
        this.address = address;
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }
}
