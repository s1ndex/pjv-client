package gui;

import gui.frames.LoginFrame;

import javax.swing.*;

public class ClientApp {

    public static void main(String[] args) {

        SwingUtilities.invokeLater(() -> new LoginFrame());
    }
}
