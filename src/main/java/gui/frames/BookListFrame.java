package gui.frames;

import connection.ClientConnection;
import gui.components.BookListPanel;
import gui.components.BookListTable;
import gui.controller.Controller;
import gui.listeners.BookTableListener;
import gui.listeners.RefreshTableListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Logger;

public class BookListFrame extends JFrame {

    private final static Logger LOGGER = Logger.getLogger(BookListFrame.class.getName());

    private BookListPanel bookListPanel;
    private BookListTable bookListTable;

    public BookListFrame() {
        super("Book List");

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            LOGGER.severe("CLIENT: Can't load default system look");
        }

        setLayout(new BorderLayout());

        bookListPanel = new BookListPanel();
        bookListTable = new BookListTable();

        bookListTable.setBookTableListener(new BookTableListener() {
            @Override
            public void rowBorrowed(int bookId) {
                try {
                    Controller.borrowBook(bookId);
                } catch (IOException e) {
                    LOGGER.severe("CLIENT: Error while sending data to the server");
                }
            }

            @Override
            public void rowReturned(int bookId) {}
        });

        bookListPanel.setGoFrameListener(event -> {
            Controller.hideBookList();

            if (Controller.getBorrowList() != null) {
                Controller.showBorrowList();
            } else {
                try {
                    Controller.setBorrowList(Controller.openBorrowList());
                } catch (Exception ex) {
                    LOGGER.severe("CLIENT: Can not connect to the server!");
                }
            }
        });

        bookListPanel.setRefreshTableListener(
                new RefreshTableListener() {
                    @Override
                    public void refreshBookTable() {
                        try {
                            bookListTable.refresh();
                        } catch (Exception ex) {
                            LOGGER.severe("CLIENT: Can not connect to the server!");
                        }
                    }

                    @Override
                    public void refreshBorrowTable() {
                    }
                });

        add(bookListPanel, BorderLayout.WEST);
        add(bookListTable, BorderLayout.CENTER);

        setMinimumSize(new Dimension(500,400));
        setSize(800, 500);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    ClientConnection.sendRequest("QUIT");
                    ClientConnection.disconnect();
                    ClientConnection.closeStreams();
//                    super.windowClosing(e);
                    System.exit(0);
                } catch (IOException ex) {
                    LOGGER.severe("CLIENT: Connection was lost");
                }
            }
        });

        setVisible(true);
        setLocationRelativeTo(null);

//        bookListTable.refresh();
    }
}
