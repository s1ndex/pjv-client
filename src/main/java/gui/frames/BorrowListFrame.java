package gui.frames;

import connection.ClientConnection;
import gui.components.BorrowListPanel;
import gui.components.BorrowListTable;
import gui.controller.Controller;
import gui.listeners.BookTableListener;
import gui.listeners.RefreshTableListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Logger;

public class BorrowListFrame extends JFrame {

    private final static Logger LOGGER = Logger.getLogger(BorrowListFrame.class.getName());

    private BorrowListPanel borrowListPanel;
    private BorrowListTable borrowListTable;

    public BorrowListFrame() throws IOException, ClassNotFoundException {
        super("Borrow List");

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            LOGGER.severe("CLIENT: Can't load default system look");
        }

        setLayout(new BorderLayout());

        Controller.setBorrowList(this);
        borrowListPanel = new BorrowListPanel();
        borrowListTable = new BorrowListTable();

        borrowListTable.setBookTableListener(new BookTableListener() {
            @Override
            public void rowBorrowed(int bookId) {
            }

            @Override
            public void rowReturned(int bookId) {
                try {
                    Controller.returnBook(bookId);
                } catch (IOException e) {
                    LOGGER.severe("CLIENT: Error while sending data to the server");
                }
            }
        });

        borrowListPanel.setGoFrameListener(event -> {
            Controller.hideBorrowList();
            Controller.showBookList();
        });

        borrowListPanel.setRefreshTableListener(
                new RefreshTableListener() {
                    @Override
                    public void refreshBookTable() {

                    }

                    @Override
                    public void refreshBorrowTable() {
                        try {
                            borrowListTable.refresh();
                        } catch (Exception ex) {
                            LOGGER.severe("CLIENT: Can not connect to the server!");
                        }
                    }
                });

        add(borrowListPanel, BorderLayout.WEST);
        add(borrowListTable, BorderLayout.CENTER);

        setMinimumSize(new Dimension(500, 400));
        setSize(800, 500);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    ClientConnection.sendRequest("QUIT");
                    ClientConnection.disconnect();
                    ClientConnection.closeStreams();
//                    super.windowClosing(e);
                    System.exit(0);
                } catch (IOException ex) {
                    LOGGER.severe("CLIENT: Connection was lost");
                }
            }
        });

        setVisible(true);
        setLocationRelativeTo(null);

//        borrowListTable.refresh();
    }
}
