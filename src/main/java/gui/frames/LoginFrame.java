package gui.frames;

import connection.ClientConnection;
import gui.components.LoginPanel;
import gui.controller.Controller;
import model.Book;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

public class LoginFrame extends JFrame {

    private final static Logger LOGGER = Logger.getLogger(LoginFrame.class.getName());

    private Preferences preferences;
    private LoginPanel loginPanel;

    public LoginFrame() {
        super("Log In");

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            LOGGER.severe("CLIENT: Can't load default system look");
        }

        setLayout(new BorderLayout());

        preferences = Preferences.userRoot().node("login");
        loginPanel = new LoginPanel();

        loginPanel.setPrefsListener((username, password, address, port) -> {
            preferences.put("username", username);
            preferences.put("password", password);
            preferences.put("address", address);
            preferences.putInt("port", port);
        });

        loginPanel.setLoginListener(event -> {
            String user = event.getUsername();
            String passwd = event.getPassword();
            String addr = event.getAddress();
            int port = event.getPort();

            if (ClientConnection.isSocketAlive(addr)) {
                if (Controller.isCorrectPassword(user, passwd, addr, port)) {
                    Controller.setUserID(ClientConnection.getUserID("ID " + user));
                    List<Book> books = ClientConnection.sendBookRequest("ALL " + Controller.getUserID());
                    Controller.setDb(books);
                    Controller.openBookList();
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(
                            LoginFrame.this,
                            "Login information isn't correct",
                            "Error",
                            JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(
                        LoginFrame.this,
                        "Server isn't available at the moment.",
                        "Error",
                        JOptionPane.WARNING_MESSAGE);
            }
        });

        String username = preferences.get("username", "");
        String password = preferences.get("password", "");
        String address = preferences.get("address", "localhost");
        int port = preferences.getInt("port", 65100);
        loginPanel.setDefaults(username, password, address, port);

        add(loginPanel, BorderLayout.CENTER);

        setSize(200, 220);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setLocationRelativeTo(null);
        setResizable(false);

    }
}
